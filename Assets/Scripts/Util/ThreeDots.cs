﻿using UnityEngine;
using System.Collections;
using System.Linq;

[RequireComponent(typeof(UILabel))]
public class ThreeDots : MonoBehaviour
{
    /// <summary>
    /// The target label.
    /// </summary>
    UILabel label;

    /// <summary>
    /// The animation timer.
    /// </summary>
    float timer;

    /// <summary>
    /// Use this for initialization.
    /// </summary>
    void Start()
    {
        label = GetComponent<UILabel>();
    }

    /// <summary>
    /// Update is called once per frame.
    /// </summary>
    void Update()
    {
        if (label && (timer -= Time.deltaTime) < 0)
        {
            timer = .5f;

            var i = label.content.IndexOf('.');

            if (i == -1)
            {
                label.content += '.';
            }
            else
            {
                var n = label.content.Length - i;
                if (++n > 3) n = 0;

                label.content = label.content.Remove(i) + new string('.', n);
            }
        }
    }
}
