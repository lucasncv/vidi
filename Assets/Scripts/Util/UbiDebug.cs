﻿using UnityEngine;
using System.Collections;

public class UbiDebug : MonoBehaviour
{
    /// <summary>
    /// Update is called once per frame.
    /// </summary>
    void Update()
    {
        guiText.text = string.Empty;

        if (uOS.gateway != null)
        {
            foreach (var device in uOS.gateway.ListDevices())
                guiText.text += device.networks[0].networkAddress + "\n";
        }
    }
}
