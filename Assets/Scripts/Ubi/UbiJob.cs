﻿using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UOS;

/// <summary>
/// Encapsulates a job.
/// </summary>
public abstract class UbiJob
{
    /// <summary>
    /// Is the job done?
    /// </summary>
    public bool done { get; protected set; }

    /// <summary>
    /// Try to complete this job.
    /// </summary>
    public abstract void DoWork();

    /// <summary>
    /// A mutex.
    /// </summary>
    public readonly static Mutex mutex = new Mutex();
}

/// <summary>
/// Trading job.
/// </summary>
public class UbiTrading : UbiJob
{
    /// <summary>
    /// The old card.
    /// </summary>
    public int oldCard { get; set; }

    /// <summary>
    /// The new card.
    /// </summary>
    public int newCard { get; set; }

    /// <summary>
    /// Try to complete this job.
    /// </summary>
    public override void DoWork()
    {
        if (done) return;

        var p = new RequestParams { { "card", oldCard } };
        UbiController.main.BroadcastRequest("requestTrade", p, HandleResponse);
    }

    /// <summary>
    /// Do a trade.
    /// </summary>
    bool HandleResponse(Response R)
    {
        if (!done)
        {
            newCard = Util.ConvertOrParse<int>(R.responseData["card"]);
            done = newCard > -1;
        }

        return done;
    }

    /// <summary>
    /// Handles a request.
    /// </summary>
    public void HandleRequest(Call serviceCall, Response serviceResponse)
    {
        if (!done)
        {
            var o = serviceCall.parameters["card"];

            serviceResponse.responseData["card"] = oldCard;
            newCard = Util.ConvertOrParse<int>(o);
            done = true;
        }        
    }
}
