﻿using UnityEngine;
using System.Collections;
using UOS;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// Controls the ubiquitous aspect of the game.
/// </summary>
[RequireComponent(typeof(uOS))]
public class UbiController : MonoBehaviour, UOSApplication, Logger
{
    /// <summary>
    /// The main instance.
    /// </summary>
    public static UbiController main { get; private set; }

    /// <summary>
    /// Returns the gateway.
    /// </summary>
    public static IGateway gateway { get { return uOS.gateway; } }

    /// <summary>
    /// Returns this device.
    /// </summary>
    public static UpDevice thisDevice { get { return gateway.currentDevice; } }

    /// <summary>
    /// Use this for initialization.
    /// </summary>
    void Awake()
    {
        main = this;
    }

    /// <summary>
    /// Use this for initialization.
    /// </summary>
    void Start()
    {
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        uOS.Init(this, this);
    }

    /// <summary>
    /// Broadcast a request and handle responses. It ignores exceptions in the handler.
    /// </summary>
    public bool BroadcastRequest(string service, RequestParams parameters, System.Func<Response, bool> handler)
    {
        // builds the call
        var call = new Call("app", service);
        call.channels = 0;
        call.parameters = parameters;

        // the devices
        var devices = gateway.ListDevices();
        if (devices == null) return false;

        // broadcast it
        foreach (var device in devices)
        {
            if (device.Equals(thisDevice))
                continue;

            try
            {
                var r = gateway.CallService(device, call);
                r.messageContext.callerDevice = device;
                
                if (string.IsNullOrEmpty(r.error) && handler(r))
                    return true;
            }
            catch { }
        }

        return false;
    }

    /// <summary>
    /// A trade has been requested.
    /// </summary>
    public void RequestTrade(Call serviceCall, Response serviceResponse, CallContext messageContext)
    {
        UbiJob.mutex.WaitOne();
        {
            serviceResponse.AddParameter("card", -1);
            var job = GameState.currentJob as UbiTrading;

            if (job != null)
                job.HandleRequest(serviceCall, serviceResponse);  
        }
        UbiJob.mutex.ReleaseMutex();
    }

    /// <summary>
    /// Initialize uOS.
    /// </summary>
    void UOSApplication.Init(IGateway gateway, uOSSettings settings)
    {
    }

    /// <summary>
    /// Tear down uOS.
    /// </summary>
    void UOSApplication.TearDown()
    {
    }

    #region Logger
    void Logger.Log(object message)
    {
        Debug.Log(message);
    }

    void Logger.LogError(object message)
    {
        //Debug.LogError(message);
    }

    void Logger.LogException(System.Exception exception)
    {
        //Debug.LogException(exception);
    }

    void Logger.LogWarning(object message)
    {
        //Debug.LogWarning(message);
    }
    #endregion
}

/// <summary>
/// Encapsulates request parameters.
/// </summary>
public class RequestParams : Dictionary<string, object> { }
