﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UOS;

/// <summary>
/// The challenge driver. Hosts a challenge.
/// </summary>
public class ChallengeHostDriver : MonoBehaviour, UOSEventDriver
{
    /// <summary>
    /// The driver ID.
    /// </summary>
    public const string DRIVER_ID = "vidi.ChallengeHostDriver";

    /// <summary>
    /// Pool complete event.
    /// </summary>
    public const string EVENT_POOL_READY = "POOL_READY";

    /// <summary>
    /// Game over event.
    /// </summary>
    public const string EVENT_GAME_OVER = "GAME_OVER";

    /// <summary>
    /// Time to wait for more players.
    /// </summary>
    public float joiningDuration = 1f;

    /// <summary>
    /// Maximum time to wait for a player to choose a card.
    /// </summary>
    public float maximumWaitTime = 30f;

    /// <summary>
    /// The main instance.
    /// </summary>
    public static ChallengeHostDriver main { get; private set; }

    /// <summary>
    /// Is the challenge running?
    /// </summary>
    public bool isHosting { get; private set; }

    /// <summary>
    /// Is the game closed?
    /// </summary>
    bool isClosed = false;

    /// <summary>
    /// The players.
    /// </summary>
    Dictionary<string, Player> players;

    /// <summary>
    /// The number of players that are ready.
    /// </summary>
    int playersReady;

    /// <summary>
    /// The card pool.
    /// </summary>
    List<int> cardPool;

    /// <summary>
    /// A timer.
    /// </summary>
    float timer;

    /// <summary>
    /// Called before all else.
    /// </summary>
    void Awake()
    {
        main = this;
    }

    /// <summary>
    /// Starts hosting.
    /// </summary>
    public void StartHosting()
    {
        isHosting = true;
        isClosed = false;
        players = new Dictionary<string, Player>();
        cardPool = new List<int>();
        playersReady = 0;
        timer = 0f;

        Debug.Log("Now hosting a challenge.");
    }

    /// <summary>
    /// Stops hosting.
    /// </summary>
    public void StopHosting()
    {
        if (isHosting)
        {
            Debug.Log("Stopped hosting a challenge.");
            isHosting = false;

            var no = new Notify(EVENT_GAME_OVER, DRIVER_ID);
            NotifyPlayers(no);
        }
    }

    /// <summary>
    /// Update.
    /// </summary>
    void Update()
    {
        if (isHosting)
        {
            if (isClosed)
            {
                if (playersReady >= players.Count)
                    EndGame();
                //else if ((timer += Time.deltaTime) > maximumWaitTime)
                // kick players
            }
            else if (players.Count >= 2 && playersReady >= players.Count)
            {
                if ((timer += Time.deltaTime) > joiningDuration)
                    StartGame();
            }
        }
    }

    /// <summary>
    /// Starts the guessing game.
    /// </summary>
    void StartGame()
    {
        Debug.Log("The challenge has started.");

        // setup
        timer = 0f;
        isClosed = true;
        playersReady = 0;

        // build pool
        cardPool.Add(GameState.selectedCard);

        foreach (var player in players.Values)
            cardPool.AddRange(player.cards);

        // notify players
        Notify no = new Notify(EVENT_POOL_READY, DRIVER_ID);
        no.AddParameter("pool", cardPool);
        NotifyPlayers(no);        
    }

    /// <summary>
    /// Ends the game.
    /// </summary>
    void EndGame()
    {
        Debug.Log("The challenge has ended.");

        int totalCorrect = 0;
        int totalGuesses = 0;
        isHosting = false;

        // players
        foreach (var player in players.Values)
        {
            var prize = new List<int>();

            // get cards from players that guessed this player's card
            foreach (var other in players.Values)
            {
                if (other != player)
                {
                    var n = other.guesses.Count(i => player.cards.Contains(i));
                    prize.AddRange(other.cards.Take(n));
                }
            }

            // guessed correctly?
            var correct = player.guesses.Count(i => i == GameState.selectedCard);

            if (correct > 0)
                prize.Add(GameState.selectedCard);

            totalCorrect += correct;
            totalGuesses += player.guesses.Count;

            // notify
            var no = new Notify(EVENT_GAME_OVER, DRIVER_ID);
            no.AddParameter("prize", prize);
            uOS.gateway.Notify(no, player.device);
        }

        // good word?
        var p = (float)totalCorrect / totalGuesses;

        if (p >= .4f && p <= .6f)
        {
            cardPool.Remove(GameState.selectedCard);
            foreach (var card in cardPool) Deck.Add(card);
            GameState.newestCards = cardPool.ToArray();
        }
        else
        {
            GameState.newestCards = null;
        }
    }

    /// <summary>
    /// Gets the word.
    /// </summary>
    public void GetWord(Call serviceCall, Response serviceResponse, CallContext messageContext)
    {
        if (!isHosting)
        {
            serviceResponse.error = "The game is not running.";
        }
        else
        {
            var device = messageContext.callerDevice;
            string key = device.name.ToLower();

            if (!players.ContainsKey(key))
                serviceResponse.error = "Player not registered.";
            else
                serviceResponse.AddParameter("word", GameState.selectedWord);
        }
    }

    /// <summary>
    /// Adds a card to the pool.
    /// </summary>
    public void AddCard(Call serviceCall, Response serviceResponse, CallContext messageContext)
    {
        Debug.Log("AddCard");

        if (!isHosting)
        {
            serviceResponse.error = "The game is not running.";
        }
        else if (isClosed)
        {
            serviceResponse.error = "The game has already begun.";
        }
        else
        {
            var device = messageContext.callerDevice;
            string key = device.name.ToLower();

            if (!players.ContainsKey(key))
            {
                serviceResponse.error = "Player not registered.";
            }
            else
            {
                var player = players[key];
                var o = serviceCall.parameters["card"];
                
                if (!PlayerSentCards(player.cards, o))
                    serviceResponse.error = "Invalid card parameter.";
            }
        }
    }

    /// <summary>
    /// Guess a card
    /// </summary>
    public void GuessCard(Call serviceCall, Response serviceResponse, CallContext messageContext)
    {
        if (!isHosting)
        {
            serviceResponse.error = "The game is not running.";
        }
        else if (!isClosed)
        {
            serviceResponse.error = "The game has not yet begun.";
        }
        else
        {
            var device = messageContext.callerDevice;
            string key = device.name.ToLower();

            if (!players.ContainsKey(key))
            {
                serviceResponse.error = "Player not registered.";
            }
            else
            {
                var player = players[key];
                var o = serviceCall.parameters["card"];

                if (!PlayerSentCards(player.guesses, o))
                    serviceResponse.error = "Invalid card parameter.";
            }
        }
    }

    /// <summary>
    /// A player has added or guessed cards.
    /// </summary>
    bool PlayerSentCards(List<int> list, object o)
    {
        var n = list.Count;

        if (o is System.Int64)
        {
            list.Add((int)(System.Int64)o);
        }
        else if (o is List<object>)
        {
            list.AddRange(((List<object>)o).OfType<System.Int64>().Cast<int>());
        }
        else
        {
            return false;
        }

        if (list.Count == n)
            return false;

        if (n == 0)
        {
            ++playersReady;
            Debug.Log("A player is now ready.");
        }

        return true;
    }

    /// <summary>
    /// A player has quit.
    /// </summary>
    void PlayerHasQuit(string key)
    {
        Player player;

        if (players.TryGetValue(key, out player))
        {
            if (isClosed)
            {
                if (player.guesses.Count > 0)
                    --playersReady;
            }
            else
            {
                if (player.cards.Count > 0)
                    --playersReady;
            }

            cardPool.AddRange(player.cards);
            players.Remove(key);
        }
    }

    #region uOS
    /// <summary>
    /// Register a player.
    /// </summary>
    public void RegisterListener(Call serviceCall, Response serviceResponse, CallContext messageContext)
    {
        Debug.Log("A player is connecting.");

        if (!isHosting)
        {
            serviceResponse.error = "The game is not running.";
        }
        else if (isClosed)
        {
            serviceResponse.error = "The game has already begun.";
        }
        else
        {
            var device = messageContext.callerDevice;
            string key = device.name.ToLower();

            if (players.ContainsKey(key))
                serviceResponse.error = "Player already registered.";
            else
            {
                players[key] = new Player { device = device };
                Debug.Log("A player connected.");
            }
        }
    }

    /// <summary>
    /// Unregister a player.
    /// </summary>
    public void UnregisterListener(Call serviceCall, Response serviceResponse, CallContext messageContext)
    {
        if (isHosting)
        {
            var device = messageContext.callerDevice;
            string key = device.name.ToLower();

            if (!players.ContainsKey(key))
                serviceResponse.error = "Player not registered.";
            else
                PlayerHasQuit(key);
        }
        else
        {
            serviceResponse.error = "The game is not running.";
        }
    }

    /// <summary>
    /// The driver instance.
    /// </summary>
    static UpDriver driverInstance;

    /// <summary>
    /// Gets the driver.
    /// </summary>
    /// <returns></returns>
    public UpDriver GetDriver()
    {
        if (driverInstance == null)
        {
            driverInstance = new UpDriver(DRIVER_ID);

            driverInstance.AddService("getWord");
            driverInstance.AddService("addCard").AddParameter("card", UpService.ParameterType.MANDATORY);
            driverInstance.AddService("guessCard").AddParameter("card", UpService.ParameterType.MANDATORY);

            driverInstance.AddService("registerListener");
            driverInstance.AddService("unregisterListener");
        }

        return driverInstance;
    }

    /// <summary>
    /// Gets this driver's parents.
    /// </summary>
    public System.Collections.Generic.List<UpDriver> GetParent()
    {
        return null;
    }

    /// <summary>
    /// Initialized.
    /// </summary>
    public void Init(IGateway gateway, uOSSettings settings, string instanceId)
    {
        enabled = true;
    }

    /// <summary>
    /// Destroyed.
    /// </summary>
    public void Destroy()
    {
        enabled = false;
    }

    /// <summary>
    /// Notifies players.
    /// </summary>
    void NotifyPlayers(Notify notify)
    {
        foreach (var device in players.Values.Select(p => p.device))
            uOS.gateway.Notify(notify, device);
    }
    #endregion

    /// <summary>
    /// A player.
    /// </summary>
    class Player
    {
        /// <summary>
        /// The player's device.
        /// </summary>
        public UpDevice device;

        /// <summary>
        /// The guesses.
        /// </summary>
        public List<int> guesses = new List<int>();

        /// <summary>
        /// The cards.
        /// </summary>
        public List<int> cards = new List<int>();
    }
}
