﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UOS;

public class ChallengeClient : MonoBehaviour, UOSEventListener
{
    /// <summary>
    /// The main instance.
    /// </summary>
    public static ChallengeClient main { get; private set; }

    /// <summary>
    /// Has joined a game?
    /// </summary>
    public bool hasJoined { get { return host != null; } }

    /// <summary>
    /// Has a card pool?
    /// </summary>
    public bool hasStarted { get { return hasJoined && cardPool != null; } }

    /// <summary>
    /// Has already guessed?
    /// </summary>
    public bool hasGuessed { get; private set; }

    /// <summary>
    /// Is the game over?
    /// </summary>
    public bool isOver { get; private set; }

    /// <summary>
    /// The current host.
    /// </summary>
    UpDevice host;

    /// <summary>
    /// The card pool.
    /// </summary>
    List<int> cardPool;

    /// <summary>
    /// Called before all else.
    /// </summary>
    void Awake()
    {
        main = this;
    }

    /// <summary>
    /// Joins the first game found.
    /// </summary>
    public bool TryToJoinGame()
    {
        if (!hasJoined)
        {
            foreach (var device in uOS.gateway.ListDevices())
            {
                if (device.Equals(uOS.gateway.currentDevice))
                    continue;

                try
                {
                    uOS.gateway.Register(this, device, ChallengeHostDriver.DRIVER_ID);
                    Debug.Log("ChallengeClient registered.");

                    var r = uOS.gateway.CallService(device, new Call(ChallengeHostDriver.DRIVER_ID, "getWord"));

                    if (!string.IsNullOrEmpty(r.error))
                        throw new System.Exception();

                    GameState.selectedWord = r.responseData["word"] as string;
                    host = device;
                    isOver = false;
                    hasGuessed = false;
                    break;
                }
                catch
                {
                    try { uOS.gateway.Unregister(this); }
                    catch { }
                }
            }
        }

        return hasJoined;
    }

    /// <summary>
    /// Quits the game.
    /// </summary>
    public void QuitGame()
    {
        if (hasJoined)
        {
            try { uOS.gateway.Unregister(this); }
            catch { }

            host = null;
        }
    }

    /// <summary>
    /// Choose a card that matches the word.
    /// </summary>
    public void ChooseCard(int card)
    {
        var call = new Call(ChallengeHostDriver.DRIVER_ID, "addCard");
        call.AddParameter("card", card);

        try
        {
            uOS.gateway.CallService(host, call);
        }
        catch { }
    }

    /// <summary>
    /// Guess the original card.
    /// </summary>
    public void GuessCard(int card)
    {
        var call = new Call(ChallengeHostDriver.DRIVER_ID, "guessCard");
        call.AddParameter("card", card);
        hasGuessed = true;

        try
        {
            uOS.gateway.CallService(host, call);
        }
        catch { }
    }

    /// <summary>
    /// Gets the cards in the game.
    /// </summary>
    public IList<int> cardsInGame
    {
        get { return cardPool; }
    }

    #region uOS
    /// <summary>
    /// Notification received.
    /// </summary>
    void UOSEventListener.HandleEvent(Notify no)
    {
        Debug.Log("EVENT");
        if (no.eventKey == ChallengeHostDriver.EVENT_POOL_READY)
        {
            var os = (List<object>)no.parameters["pool"];
            cardPool = new List<int>();

            foreach (var o in os)
                cardPool.Add(Util.ConvertOrParse<int>(o));
        }
        else if (no.eventKey == ChallengeHostDriver.EVENT_GAME_OVER)
        {
            object o;

            if (no.parameters.TryGetValue("prize", out o))
            {
                var list = new List<int>();

                foreach (var i in (List<object>)o)
                    list.Add(Util.ConvertOrParse<int>(i));

                GameState.newestCards = list.ToArray();

                foreach (var card in GameState.newestCards)
                    Deck.Add(card);
            }
            else
            {
                GameState.newestCards = null;
            }

            isOver = true;
        }
    }
    #endregion
}
