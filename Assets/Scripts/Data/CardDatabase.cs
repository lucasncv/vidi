﻿using System;
using System.IO;
#if UNITY_EDITOR
using UnityEditor;
#endif

using UnityEngine;

[Serializable]
public class CardDatabase : ScriptableObject
{
    /// <summary>
    /// The cards.
    /// </summary>
    public Texture2D[] cards;

    /// <summary>
    /// Gets a texture.
    /// </summary>
    public Texture2D this[int index]
    {
        get { return index < cards.Length ? cards[index] : null; }
    }

    /// <summary>
    /// The card aspect ratio.
    /// </summary>
    public float aspectRatio
    {
        get { return (float)cards[0].width / cards[0].height; }
    }

#if UNITY_EDITOR
    /// <summary>
    /// Menu entry.
    /// </summary>
    [MenuItem("Assets/Create/Card Database", false, 9000)]
    static void Create()
    {
        var path = AssetDatabase.GetAssetPath(Selection.activeObject);

        if (path == "")
            path = "Assets";
        else if (Path.GetExtension(path) != "")
            path = path.Replace(Path.GetFileName(path), "");

        string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath(path + "/Database.asset");

        var asset = ScriptableObject.CreateInstance<CardDatabase>();
        AssetDatabase.CreateAsset(asset, assetPathAndName);
        Selection.activeObject = asset;
    }
#endif
}
