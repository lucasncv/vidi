﻿using System;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A deck.
/// </summary>
public static class Deck
{
    /// <summary>
    /// The list of cards.
    /// </summary>
    static int[] cards;

    /// <summary>
    /// Check if the deck has a card.
    /// </summary>
    public static bool Has(int id)
    {
        if (cards == null) Load();
        return id < cards.Length && cards[id] > 0;
    }

    /// <summary>
    /// Gets a new card.
    /// </summary>
    public static void Add(int id)
    {
        if (cards == null) Load();

        if (id >= cards.Length)
            Array.Resize(ref cards, id + 1);

        cards[id] += 1;
        Save();
    }

    /// <summary>
    /// Removes a card.
    /// </summary>
    public static void Remove(int id)
    {
        if (Has(id))
        {
            cards[id] -= 1;
            Save();
        }
    }

    /// <summary>
    /// Enumerate through all cards.
    /// </summary>
    public static IEnumerable<int> Enumerate()
    {
        if (cards == null) Load();

        for (int i = 0; i < cards.Length; ++i)
        {
            for (int k = 0; k < cards[i]; ++k)
                yield return i;
        }
    }

    /// <summary>
    /// Loads the deck.
    /// </summary>
    static void Load()
    {
        cards = new int[64];

        for (int i = 0; i < 12; ++i)
            Add(UnityEngine.Random.Range(0, 100));

        return;
        var data = PlayerPrefs.GetString("Deck");

        try
        {
            if (!string.IsNullOrEmpty(data))
            {
                var bytes = Convert.FromBase64String(data);
                cards = new int[BitConverter.ToInt32(bytes, 0)];

                for (int i = 0; i < cards.Length; ++i)
                    cards[i] = BitConverter.ToInt32(bytes, 4 * (i + 1));
                

                return;
            }
        }
        catch
        {
        }

        cards = new int[0];
    }

    /// <summary>
    /// Saves the deck.
    /// </summary>
    static void Save()
    {
        var data = new byte[cards.Length * 4 + 4];

        Buffer.BlockCopy(BitConverter.GetBytes(cards.Length), 0, data, 0, 4);
        Buffer.BlockCopy(cards, 0, data, 4, data.Length - 4);

        //PlayerPrefs.SetString("Deck", Convert.ToBase64String(data));
    }
}
