﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UOS;

public class UIController : MonoBehaviour
{
    /// <summary>
    /// The main controller.
    /// </summary>
    public static UIController main { get; private set; }

    public UICardGrid cardSelection;
    public UIButton cardSelectionButton;

    public UITextField wordSelection;
    public UIButton wordSelectionButton;

    public UICard selectedCard;
    public UILabel selectedWord;

    public UICardGrid newestCards;

    public bool tryToTrade;

    public float networkInterval = 1f;

    float networkCooldown;

    /// <summary>
    /// A card has been selected.
    /// </summary>
    void OnCardSelected()
    {
        if (cardSelection && cardSelection.selection > -1)
        {
            GameState.selectedCard = cardSelection.selection;

            if (gameObject.name == "Start Challenge")
            {
                ChangePage("Prepare Challenge");
            }
            else if (gameObject.name == "Start Trade")
            {
                ChangePage("Wait Trade");
            }
            else if (gameObject.name == "Do Challenge")
            {
                if (ChallengeClient.main.hasStarted)
                    ChallengeClient.main.GuessCard(GameState.selectedCard);
                else
                    ChallengeClient.main.ChooseCard(GameState.selectedCard);

                ChangePage("Wait Challenge");
            }
        }
    }

    /// <summary>
    /// The word has been selected.
    /// </summary>
    void OnWordSelected()
    {
        if (wordSelection && !string.IsNullOrEmpty(wordSelection.content))
        {
            GameState.selectedWord = wordSelection.content;
            ChallengeHostDriver.main.StartHosting();
            wordSelection.ChangePage("Wait Challenge");
        }
    }

    /// <summary>
    /// Called once per frame.
    /// </summary>
    void Update()
    {
        if (cardSelection && cardSelectionButton)
            cardSelectionButton.enabled = cardSelection.selection > -1;

        if (wordSelection && wordSelectionButton)
            wordSelectionButton.enabled = !string.IsNullOrEmpty(wordSelection.content);

        if (networkCooldown <= 0)
        {
            networkCooldown = networkInterval;
            UpdateJob();
        }
        else
        {
            networkCooldown -= Time.deltaTime;
        }
    }

    /// <summary>
    /// Make network requests.
    /// </summary>
    void UpdateJob()
    {
        UbiJob.mutex.WaitOne();

        if (GameState.currentJob != null)
        {
            if (!GameState.currentJob.done)
            {
                GameState.currentJob.DoWork();
            }
            else if (GameState.currentJob is UbiTrading)
            {
                var job = GameState.currentJob as UbiTrading;
                GameState.currentJob = null;

                GotSomeCards(job.newCard);
                Deck.Remove(job.oldCard);
            }
        }

        UbiJob.mutex.ReleaseMutex();

        // challenge
        if (gameObject.name == "Procurando")
        {
            if (ChallengeClient.main.TryToJoinGame())
                ChangePage("Do Challenge");
        }
        else if (gameObject.name == "Wait Challenge")
        {
            if (ChallengeClient.main.hasJoined)
            {
                if (ChallengeClient.main.hasGuessed)
                {
                    if (ChallengeClient.main.isOver)
                    {
                        ChangePage("Got Cards");
                        ChallengeClient.main.QuitGame();
                    }
                }
                else if (ChallengeClient.main.hasStarted)
                {
                    ChangePage("Do Challenge");
                }
            }
            else if (!ChallengeHostDriver.main.isHosting)
            {
                ChangePage("Got Cards");
            }
        }
    }

    /// <summary>
    /// Got some new cards.
    /// </summary>
    void GotSomeCards(params int[] cards)
    {
        GameState.newestCards = new int[cards.Length];

        for (int i = 0; i < cards.Length; ++i)
        {
            Deck.Add(cards[i]);
            GameState.newestCards[i] = cards[i];
        }

        ChangePage("Got Cards");
    }

    /// <summary>
    /// Query if the main instance is active.
    /// </summary>
    public static bool isActive
    {
        get { return main && main.gameObject.activeInHierarchy && main.enabled; }
    }

    /// <summary>
    /// Called when enabled.
    /// </summary>
    void OnEnable()
    {
        main = this;

        if (selectedCard)
            selectedCard.id = GameState.selectedCard;

        if (selectedWord)
            selectedWord.content = GameState.selectedWord;

        if (cardSelection)
        {
            if (gameObject.name == "Do Challenge" && ChallengeClient.main.hasStarted)
                cardSelection.list = ChallengeClient.main.cardsInGame;
            else
                cardSelection.list = Deck.Enumerate().ToList();
        }

        if (newestCards)
            newestCards.list = GameState.newestCards;

        // cancel challenge
        if (gameObject.name == "Main Menu")
        {
            if (ChallengeClient.main)
                ChallengeClient.main.QuitGame();

            if (ChallengeHostDriver.main)
                ChallengeHostDriver.main.StopHosting();
        }

        CreateJob();
    }

    /// <summary>
    /// Called when disabled.
    /// </summary>
    void OnDisable()
    {
        GameState.currentJob = null;
    }

    /// <summary>
    /// Creates a new job.
    /// </summary>
    void CreateJob()
    {
        UbiJob.mutex.WaitOne();

        if (gameObject.name == "Wait Trade")
        {
            var job = new UbiTrading();
            job.oldCard = GameState.selectedCard;
            GameState.currentJob = job;
        }
 
        UbiJob.mutex.ReleaseMutex();
    }

    /// <summary>
    /// Changes the page.
    /// </summary>
    void ChangePage(string page)
    {
        var widget = GetComponentInChildren<UIWidget>();
        widget.ChangePage(page);
    }
}
