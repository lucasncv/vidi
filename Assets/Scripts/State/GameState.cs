﻿using UnityEngine;
using System.Collections;

/// <summary>
/// The game state.
/// </summary>
public static class GameState
{
    /// <summary>
    /// The selected card.
    /// </summary>
    public static int selectedCard;

    /// <summary>
    /// The selected word.
    /// </summary>
    public static string selectedWord;

    /// <summary>
    /// Newest cards.
    /// </summary>
    public static int[] newestCards;

    /// <summary>
    /// The current job.
    /// </summary>
    public static UbiJob currentJob;
}
