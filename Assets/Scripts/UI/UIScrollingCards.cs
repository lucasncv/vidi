﻿using UnityEngine;
using System.Collections;

public class UIScrollingCards : UIWidget
{
    /// <summary>
    /// The card database.
    /// </summary>
    [Header("Scrolling cards")]
    public CardDatabase db;

    /// <summary>
    /// The unfocused card scaling.
    /// </summary>
    public float cardScaling = .5f;

    /// <summary>
    /// The transition duration.
    /// </summary>
    public float transitionDuration = 1f;

    /// <summary>
    /// The wait duration.
    /// </summary>
    public float waitDuration = 3f;

    /// <summary>
    /// The scroll position.
    /// </summary>
    float offset;

    /// <summary>
    /// Current focused card.
    /// </summary>
    int focus = 0;

    /// <summary>
    /// The cooldown.
    /// </summary>
    float cooldown = 0f;

    /// <summary>
    /// Called before all else.
    /// </summary>
    protected override void Start()
    {
        if (!db) enabled = false;
    }

    /// <summary>
    /// Called when enabled.
    /// </summary>
    void OnEnable()
    {
        offset = 0f;
        cooldown = waitDuration;
        focus = Random.Range(0, db.cards.Length - 1);
    }

    /// <summary>
    /// Updates this widget.
    /// </summary>
    protected override void Update()
    {
        base.Update();

        if (cooldown > 0f)
            cooldown -= Time.deltaTime;
        else
            offset += Time.deltaTime / transitionDuration;

        int d = (int)offset;

        if (d != 0)
        {
            offset -= d;
            focus = NextCard(d);

            cooldown = waitDuration;
        }
    }

    /// <summary>
    /// Draws this widget.
    /// </summary>
    public override void Draw()
    {
        if (!db) return;

        // draw focus
        DrawFocusedCard();
        DrawFocusingCard();
        DrawFadingOutCard();
        DrawFadingInCard();
    }

    /// <summary>
    /// Gets another card.
    /// </summary>
    int NextCard(int delta)
    {
        delta += focus;
        delta %= db.cards.Length;

        if (delta < 0)
            delta += db.cards.Length;

        return delta;
    }

    /// <summary>
    /// Draws the focusing card.
    /// </summary>
    void DrawFocusedCard()
    {
        var area = drawingArea;

        area.x = area.center.x;
        area.y = area.yMax;

        area.x -= Mathf.SmoothStep(0f, firstSpacing, offset);

        area.height *= Mathf.SmoothStep(1f, cardScaling, offset);
        area.width = area.height * db.aspectRatio;

        area.x -= area.width / 2f;
        area.y -= area.height;

        UITexture.Draw(db[focus], area, new Rect(0, 0, 1, 1), tint);
    }

    /// <summary>
    /// Draws the focused card.
    /// </summary>
    void DrawFocusingCard()
    {
        var area = drawingArea;

        area.x = area.center.x;
        area.y = area.yMax;

        area.x += Mathf.SmoothStep(firstSpacing, 0f, offset);

        area.height *= Mathf.SmoothStep(cardScaling, 1f, offset);
        area.width = area.height * db.aspectRatio;

        area.x -= area.width / 2f;
        area.y -= area.height;

        UITexture.Draw(db[NextCard(1)], area, new Rect(0, 0, 1, 1), tint);
    }

    /// <summary>
    /// Draws the card.
    /// </summary>
    void DrawFadingOutCard()
    {
        var area = drawingArea;

        area.x = area.center.x;
        area.y = area.yMax;

        area.x -= firstSpacing + Mathf.SmoothStep(0f, secondSpacing, offset);

        area.height *= cardScaling;
        area.width = area.height * db.aspectRatio;

        area.x -= area.width / 2f;
        area.y -= area.height;

        UITexture.Draw(db[NextCard(-1)], area, new Rect(0, 0, 1, 1), tint.Multiply(1 - offset));
    }

    /// <summary>
    /// Draws the card.
    /// </summary>
    void DrawFadingInCard()
    {
        var area = drawingArea;

        area.x = area.center.x;
        area.y = area.yMax;

        area.x += firstSpacing + Mathf.SmoothStep(secondSpacing, 0f, offset);

        area.height *= cardScaling;
        area.width = area.height * db.aspectRatio;

        area.x -= area.width / 2f;
        area.y -= area.height;

        UITexture.Draw(db[NextCard(2)], area, new Rect(0, 0, 1, 1), tint.Multiply(offset));
    }

    /// <summary>
    /// Gets the first spacing.
    /// </summary>
    float firstSpacing
    {
        get
        {
            var w = drawingArea.height * db.aspectRatio;
            return (w + w * cardScaling) * .5f + .2f;
        }
    }

    /// <summary>
    /// Gets the second spacing.
    /// </summary>
    float secondSpacing
    {
        get { return drawingArea.height * db.aspectRatio * cardScaling + .2f; }
    }
}
