﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Show a grid of cards.
/// </summary>
public class UICardGrid : UIWidget
{
    /// <summary>
    /// The card database.
    /// </summary>
    [Header("Card grid")]
    public CardDatabase db;

    /// <summary>
    /// The height of the cards.
    /// </summary>
    public float cardHeight = 1f;

    /// <summary>
    /// The selection event.
    /// </summary>
    public string selectEvent;

    /// <summary>
    /// The list of cards.
    /// </summary>
    public IList<int> list { get; set; }

    /// <summary>
    /// The current selection.
    /// </summary>
    public int selection { get; private set; }

    /// <summary>
    /// The card width.
    /// </summary>
    float cardWidth { get { return db.aspectRatio * cardHeight; } }

    /// <summary>
    /// Grid size.
    /// </summary>
    int columns, rows;

    /// <summary>
    /// Spacing between cards.
    /// </summary>
    Vector2 spacing;

    /// <summary>
    /// The initial mouse position.
    /// </summary>
    Vector2 initialMousePosition;

    /// <summary>
    /// The current page.
    /// </summary>
    int currentPage;

    /// <summary>
    /// The page count.
    /// </summary>
    int pageCount { get { return Mathf.CeilToInt((float)list.Count / (columns * rows)); } }

    /// <summary>
    /// The offset between pages.
    /// </summary>
    float pageOffset;

    /// <summary>
    /// The page recover speed.
    /// </summary>
    float pageSpeed;

    /// <summary>
    /// The selection animation.
    /// </summary>
    float selectionFX;

    /// <summary>
    /// Called before all else.
    /// </summary>
    protected override void Start()
    {
        if (!db)
        {
            enabled = false;
            return;
        }
    }

    /// <summary>
    /// Called when enabled.
    /// </summary>
    void OnEnable()
    {
        selection = -1;
    }

    /// <summary>
    /// Arranges the cards.
    /// </summary>
    void Arrange()
    {
        var rect = drawingArea;

        columns = (int)(rect.width / cardWidth);
        spacing.x = (rect.width - columns * cardWidth) / (columns + 1);

        rows = (int)(rect.height / cardHeight);
        spacing.y = (rect.height - rows * cardHeight) / (rows + 1);
    }

    /// <summary>
    /// Called once per frame.
    /// </summary>
    protected override void Update()
    {
        if (list != null)
        {
            base.Update();
            Arrange();

            if (!inputStarted)
            {
                pageOffset = Mathf.SmoothDamp(pageOffset, 0f, ref pageSpeed, .2f);
                if (Mathf.Abs(pageOffset) < .1f) pageOffset = .0f;
            }

            selectionFX = Mathf.Clamp01(selectionFX + Time.deltaTime * 10f);
        }
    }

    /// <summary>
    /// Mouse.
    /// </summary>
    protected override void OnFingerDrag(Vector2 delta)
    {
        if (selection >= 0) return;
        pageOffset += delta.x;

        var w = drawingArea.width * .5f;
        pageOffset = Mathf.Clamp(pageOffset, -w, w);
    }

    /// <summary>
    /// Mouse.
    /// </summary>
    protected override void OnFingerUp()
    {
        if (selection >= 0)
        {
            if (isMouseOver)
            {
                selection = -1;
                NotifySelectionChanged();
            }
        }
        else if (Mathf.Approximately(pageOffset, 0f) && isMouseOver)
        {
            var p = UIManager.fingerPosition;
            p -= drawingArea.position;

            var x = Mathf.FloorToInt((p.x - spacing.x) / (spacing.x + cardWidth));
            var y = Mathf.FloorToInt((p.y - spacing.y) / (spacing.y + cardHeight));

            var i = y * columns + x + currentPage * columns * rows;

            if (i >= 0 && i < list.Count)
            {
                selection = list[i];
                selectionFX = 0f;
                NotifySelectionChanged();
            }
        }
        else if (Mathf.Abs(pageOffset) >= drawingArea.width * .4f)
        {
            int d = -(int)Mathf.Sign(pageOffset);

            if (currentPage >= -d && currentPage + d < pageCount)
            {
                currentPage += d;
                pageOffset = -pageOffset;
            }
        }
    }

    /// <summary>
    /// Draws this widget.
    /// </summary>
    public override void Draw()
    {
        if (!db || list == null) return;
        var rect = drawingArea;

        if (!Application.isPlaying)
            Arrange();

        var topLeft = rect.position;
        topLeft.x += pageOffset;

        DrawPage(currentPage, topLeft);

        if (pageOffset != 0)
        {
            int d = -(int)Mathf.Sign(pageOffset);

            topLeft.x += d * rect.width;
            DrawPage(currentPage + d, topLeft);
        }

        DrawSelection();
    }

    /// <summary>
    /// The selection has changed.
    /// </summary>
    void NotifySelectionChanged()
    {
        if (!string.IsNullOrEmpty(selectEvent))
            SendMessageUpwards(selectEvent, selection, SendMessageOptions.DontRequireReceiver);
    }

    /// <summary>
    /// Draws a page.
    /// </summary>
    void DrawPage(int page, Vector2 topLeft)
    {
        if (page < 0 || currentPage >= pageCount) return;
        int i = page * rows * columns;

        float alpha = 1f - Mathf.Min(1, Mathf.Abs(drawingArea.xMin - topLeft.x) / drawingArea.width);

        if (selection > -1)
            alpha *= Mathf.SmoothStep(1f, .5f, selectionFX);

        for (int y = 0; y < rows && i < list.Count; ++y)
        {
            for (int x = 0; x < columns && i < list.Count; ++x)
            {
                var p = topLeft;

                p.x += spacing.x * (x + 1) + cardWidth * x;
                p.y += spacing.y * (y + 1) + cardHeight * y;

                DrawCard(db[list[i++]], p, alpha);
            }
        }
    }

    /// <summary>
    /// Draws a card.
    /// </summary>
    void DrawCard(Texture2D card, Vector2 position, float alpha)
    {
        var area = new Rect();

        area.position = position;
        area.height = cardHeight;
        area.width = cardWidth;

        UITexture.Draw(card, area, new Rect(0, 0, 1, 1), tint.Multiply(alpha));
    }

    /// <summary>
    /// Draws the selected card.
    /// </summary>
    void DrawSelection()
    {
        if (selection < 0) return;

        var area = drawingArea;

        area.position = area.center;
        area.height *= .5f;
        area.width = area.height * db.aspectRatio;
        area.position -= area.size / 2f;

        var alpha = Mathf.SmoothStep(0f, 1f, selectionFX);

        UITexture.Draw(db[selection], area, new Rect(0, 0, 1, 1), tint.Multiply(alpha));
    }
}
