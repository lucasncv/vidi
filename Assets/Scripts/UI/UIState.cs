﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Encapsulates the state of a widget.
/// </summary>
[System.Serializable]
public class UIState
{
    /// <summary>
    /// The position.
    /// </summary>
    public Vector2 position;

    /// <summary>
    /// The size.
    /// </summary>
    public Vector2 size;

    /// <summary>
    /// The tint color.
    /// </summary>
    public Color tint;

    /// <summary>
    /// Applies this state to a widget. Optionally transitioning from another.
    /// </summary>
    public virtual void Apply(UIWidget widget, UIState origin, float t)
    {
        if (t == 1)
        {
            widget.position = position;
            widget.size = size;
            widget.tint = tint;
        }
        else if (origin != null)
        {
            widget.position = UIExtensions.Slerp(origin.position, position, t);
            widget.size = UIExtensions.Slerp(origin.size, size, t);
            widget.tint = UIExtensions.Slerp(origin.tint, tint, t);
        }
        else
        {
            widget.position = position;
            widget.size = size * t;
            widget.tint = tint.Multiply(t);
        }
    }

    /// <summary>
    /// Creates a state from a widget.
    /// </summary>
    public static UIState From(UIWidget widget)
    {
        var state = new UIState();

        state.position = widget.position;
        state.size = widget.size;
        state.tint = widget.tint;

        return state;
    }
}

/// <summary>
/// A dictionary of states.
/// </summary>
[System.Serializable]
public class StateDictionary : Dictionary<string, UIState>, ISerializationCallbackReceiver
{
    [SerializeField]
    string[] keys;

    [SerializeField]
    UIState[] states;

    public void OnAfterDeserialize()
    {
        for (int i = 0; i < keys.Length; ++i)
            this[keys[i]] = states[i];

        keys = null;
        states = null;
    }

    public void OnBeforeSerialize()
    {
        keys = new string[Count];
        states = new UIState[Count];

        int i = 0;

        foreach (var kvp in this)
        {
            keys[i] = kvp.Key;
            states[i++] = kvp.Value;
        }
    }
}
