﻿using UnityEngine;
using System.Collections;

public class UILabel : UIWidget
{
    /// <summary>
    /// Normal background.
    /// </summary>
    [Header("Label")]
    public string content = "Label";

    /// <summary>
    /// Active background.
    /// </summary>
    public Color textColor = Color.black;

    /// <summary>
    /// The text height.
    /// </summary>
    public float textHeight = .5f;

    /// <summary>
    /// The font.
    /// </summary>
    public Font font;

    /// <summary>
    /// The text style.
    /// </summary>
    protected GUIStyle textStyle;

    /// <summary>
    /// GUI.
    /// </summary>
    protected virtual void OnGUI()
    {
        if (!CheckFont()) return;

        var rect = screenArea;
        GUI.Label(rect, content, textStyle);
    }

    /// <summary>
    /// Checks the font.
    /// </summary>
    protected virtual bool CheckFont()
    {
        if (textStyle == null)
        {
            textStyle = new GUIStyle();
            textStyle.font = font;
            textStyle.alignment = TextAnchor.MiddleCenter;
            textStyle.wordWrap = true;
            textStyle.padding = new RectOffset(8, 8, 8, 8);
        }

        var u = 10 / Mathf.Sqrt(Screen.width * Screen.height);

        textStyle.normal.textColor = textColor;
        textStyle.fontSize = (int)(textHeight / u + .5f);

        return true;
    }
}
