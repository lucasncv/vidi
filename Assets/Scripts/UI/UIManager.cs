﻿using UnityEngine;
using System.Collections.Generic;
using System.Reflection;

[ExecuteInEditMode]
public class UIManager : MonoBehaviour
{
    /// <summary>
    /// The current page.
    /// </summary>
    public Transform root;

    /// <summary>
    /// Called before updates.
    /// </summary>
    void Start()
    {
        if (!root)
            root = transform;
    }

    /// <summary>
    /// Called once per frame.
    /// </summary>
    void Update()
    {
    }

    /// <summary>
    /// Render UI.
    /// </summary>
    void OnPostRender()
    {
        if (root)
        {
            var sz = screenSize / 2;

            GL.PushMatrix();
            GL.LoadPixelMatrix(-sz.x, sz.x, sz.y, -sz.y);

            foreach (var widget in root.GetComponentsInChildren<UIWidget>())
            {
                if (widget.enabled)
                    widget.Draw();
            }

            GL.PopMatrix();
        }
    }
    
    /// <summary>
    /// Gets the screen size.
    /// </summary>
    public static Vector2 screenSize
    {
        get
        {
            var ratio = Mathf.Sqrt(Screen.width / (float)Screen.height);
            return new Vector2(ratio, 1f / ratio) * 10f;
        }
    }

    /// <summary>
    /// Gets the finger position.
    /// </summary>
    public static Vector2 fingerPosition
    {
        get
        {
            var p = (Vector2)Input.mousePosition;
            p.y = Screen.height - p.y;

            var u = 10 / Mathf.Sqrt(Screen.width * Screen.height);
            var h = new Vector2(Screen.width, Screen.height) / 2;

            return (p - h) * u;
        }
    }

    /// <summary>
    /// Pops a GUIClip.
    /// </summary>
    public static void PopClip()
    {
        var type = System.Type.GetType("UnityEngine.GUIClip,UnityEngine");
        var mi = type.GetMethod("Pop", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Static);
        mi.Invoke(null, null);
    }

    /// <summary>
    /// Reapplies a GUIClip.
    /// </summary>
    public static void ReapplyClip()
    {
        var type = System.Type.GetType("UnityEngine.GUIClip,UnityEngine");
        var mi = type.GetMethod("Reapply", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Static);
        mi.Invoke(null, null);
    }
}

/// <summary>
/// UI extensions.
/// </summary>
internal static class UIExtensions
{
    /// <summary>
    /// Gets immediate children.
    /// </summary>
    public static IEnumerable<T> GetChildren<T>(this Transform self) where T : Component
    {
        for (int i = 0; i < self.childCount; ++i)
        {
            var co = self.GetChild(i).GetComponent<T>();
            if (co) yield return co;
        }
    }

    /// <summary>
    /// Multiplies a color by an alpha value.
    /// </summary>
    public static Color Multiply(this Color self, float a)
    {
        self.a *= a;
        return self;
    }

    /// <summary>
    /// Smooth interpolation.
    /// </summary>
    public static Vector2 Slerp(Vector2 from, Vector2 to, float a)
    {
        to.x = Mathf.SmoothStep(from.x, to.x, a);
        to.y = Mathf.SmoothStep(from.y, to.y, a);
        return to;
    }

    /// <summary>
    /// Smooth interpolation.
    /// </summary>
    public static Color Slerp(Color from, Color to, float a)
    {
        to.a = Mathf.SmoothStep(from.a, to.a, a);
        to.r = Mathf.SmoothStep(from.r, to.r, a);
        to.g = Mathf.SmoothStep(from.g, to.g, a);
        to.b = Mathf.SmoothStep(from.b, to.b, a);
        return to;
    }

    /// <summary>
    /// Inflates a rectangle.
    /// </summary>
    public static Rect Inflate(this Rect self, Vector2 delta)
    {
        self.position -= delta / 2f;
        self.size += delta;
        return self;
    }

    /// <summary>
    /// Inverts a color.
    /// </summary>
    public static Color Invert(this Color self)
    {
        float a = self.a;
        self -= Color.white;
        self.a = a;
        return self;
    }

    /// <summary>
    /// Sets the clipping offset.
    /// </summary>
    public static void SetClipOffset(this GUIStyle self, Vector2 offset)
    {
        var pi = typeof(GUIStyle).GetProperty("Internal_clipOffset", BindingFlags.NonPublic | BindingFlags.Instance);
        pi.SetValue(self, offset, null);
    }

    /// <summary>
    /// A white texture.
    /// </summary>
    static Texture2D whiteTex;

    /// <summary>
    /// A white texture.
    /// </summary>
    public static Texture whiteTexture
    {
        get
        {
            if (whiteTex == null)
            {
                whiteTex = new Texture2D(1, 1, TextureFormat.ARGB32, false, false);
                whiteTex.SetPixel(0, 0, UnityEngine.Color.white);
                whiteTex.Apply();
            }

            return whiteTex;
        }
    }
}