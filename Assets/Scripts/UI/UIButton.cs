﻿using UnityEngine;
using System.Collections;

public class UIButton : UILabel
{
    /// <summary>
    /// Active background.
    /// </summary>
    [Header("Button")]
    public Color activeBackground = Color.red;

    /// <summary>
    /// The click event name.
    /// </summary>
    public string clickEvent;

    /// <summary>
    /// The event argument.
    /// </summary>
    public string eventArgument;

    /// <summary>
    /// Is the button pressed?
    /// </summary>
    bool isPressed;

    /// <summary>
    /// Called once per frame.
    /// </summary>
    protected override void Update()
    {
        base.Update();

        isPressed = inputStarted && drawingArea.Contains(UIManager.fingerPosition);
    }

    /// <summary>
    /// Mouse.
    /// </summary>
    protected override void OnFingerUp()
    {
        if (isMouseOver && !string.IsNullOrEmpty(clickEvent))
            SendMessageUpwards(clickEvent, eventArgument, SendMessageOptions.DontRequireReceiver);
    }

    /// <summary>
    /// Draws this widget.
    /// </summary>
    public override void Draw()
    {
        DrawBackground(drawingArea, (isPressed ? activeBackground : background) * tint);
    }
}
