﻿using UnityEngine;
using System.Collections;
using System.Text.RegularExpressions;

/// <summary>
/// A text field.
/// </summary>
public class UITextField : UILabel
{
    /// <summary>
    /// The maximum length.
    /// </summary>
    [Header("Text field")]
    int maxLength = 21;

#if UNITY_ANDROID && !UNITY_EDITOR
    /// <summary>
    /// The on-screen keyboard.
    /// </summary>
    TouchScreenKeyboard keyboard;

    /// <summary>
    /// Touched.
    /// </summary>
    protected override void OnFingerDown()
    {
        keyboard = TouchScreenKeyboard.Open(content, TouchScreenKeyboardType.ASCIICapable);
    }

    /// <summary>
    /// Update.
    /// </summary>
    protected override void Update()
    {
        base.Update();

        if (keyboard != null)
        {
            if (keyboard.done)
            {
                content = keyboard.text;

                if (content.Length > maxLength)
                    content = content.Remove(maxLength);

                keyboard = null;
            }
        }
    }
#else
    /// <summary>
    /// The text editor.
    /// </summary>
    TextEditor editor = new TextEditor();

     /// <summary>
    /// GUI.
    /// </summary>
    protected override void OnGUI()
    {
        Event ev = Event.current;
        UpdateEditor();

        if (ev.type == EventType.repaint)
        {
            var rect = screenArea;

            GUI.skin.settings.cursorColor = textColor;
            GUI.skin.settings.selectionColor = textColor.Invert();
            editor.style.DrawWithTextSelection(rect, new GUIContent(content), 0, editor.pos, editor.selectPos);

            GUI.Label(rect, content, textStyle);
            return;
        }

        var mouse = ev.mousePosition;
        mouse.y += textStyle.fontSize * .5f;

        switch (ev.type)
        {
            case EventType.MouseDown:
                if (editor.position.Contains(mouse))
                {
                    editor.MoveCursorToPosition(mouse);
                    ev.Use();
                }
                break;

            case EventType.MouseUp:
                editor.MouseDragSelectsWholeWords(false);
                ev.Use();
                break;

            case EventType.MouseDrag:
                if (inputStarted)
                {
                    if (ev.shift)
                        editor.MoveCursorToPosition(mouse);
                    else
                        editor.SelectToPosition(mouse);

                    ev.Use();
                }
                break;

            case EventType.KeyDown:
                if (!editor.HandleKeyEvent(ev))
                {
                    string s = "" + ev.character;

                    if (!string.IsNullOrEmpty(s) && Regex.IsMatch(s, @"^[a-zA-Z0-9]+$"))
                        editor.Insert(ev.character);
                }

                break;
        }

        content = editor.content.text;

        if (content.Length > maxLength)
        {
            editor.content.text = content = content.Remove(maxLength);
            editor.ClampPos();
        }
    }

    /// <summary>
    /// Checks the font.
    /// </summary>
    protected override bool CheckFont()
    {
        base.CheckFont();

        textStyle.wordWrap = false;
        textStyle.clipping = TextClipping.Clip;
        textStyle.contentOffset = -editor.scrollOffset;
        textStyle.SetClipOffset(-textStyle.contentOffset);

        editor.style = new GUIStyle(textStyle);
        editor.style.normal.textColor = new Color();
        editor.style.contentOffset = -Vector2.up * textStyle.fontSize * .5f - editor.scrollOffset;
        editor.style.SetClipOffset(-editor.style.contentOffset);

        return true;
    }

    /// <summary>
    /// Updates the editor.
    /// </summary>
    void UpdateEditor()
    {
        editor.content.text = content;
        editor.position = screenArea;
        CheckFont();
    }
#endif

    /// <summary>
    /// Called when enabled.
    /// </summary>
    void OnEnable()
    {
        content = string.Empty;
    }
}
