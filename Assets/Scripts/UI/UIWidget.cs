﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A basic widget.
/// </summary>
public abstract class UIWidget : MonoBehaviour
{
    /// <summary>
    /// The tint color.
    /// </summary>
    [Header("Visual")]
    public Color tint = Color.white;

    /// <summary>
    /// The background color.
    /// </summary>
    public Color background;

    /// <summary>
    /// Is the content arranged vertically?
    /// </summary>
    [Header("Layout")]
    public TextAnchor pivot = TextAnchor.MiddleCenter;

    /// <summary>
    /// The position.
    /// </summary>
    public Vector2 position
    {
        get { return transform.position; }
        set { transform.position = value; }
    }

    /// <summary>
    /// The size.
    /// </summary>
    public Vector2 size
    {
        get { return transform.localScale; }
        set { transform.localScale = new Vector3(value.x, value.y, 1f); }
    }

    /// <summary>
    /// Changes the page.
    /// </summary>
    public void ChangePage(string page)
    {
        for (var current = transform.parent; current.parent; current = current.parent)
        {
            var up = current.parent;
            var T = up.Find(page);

            if (T)
            {
                if (pageStack == null) pageStack = new Stack<Transform>();
                pageStack.Push(current);

                current.gameObject.SetActive(false);
                T.gameObject.SetActive(true);
                break;
            }
        }
    }

    /// <summary>
    /// Goes back to the previous page.
    /// </summary>
    public void GoBack()
    {
        if (pageStack != null && pageStack.Count > 0)
        {
            var T = pageStack.Pop();
            var up = T.parent;

            for (int i = 0; i < up.childCount; ++i)
            {
                var current = up.GetChild(i);

                if (transform.IsChildOf(current))
                {
                    current.gameObject.SetActive(false);
                    T.gameObject.SetActive(true);
                    break;
                }
            }
        }
    }

    /// <summary>
    /// Goes back to the first screen.
    /// </summary>
    public void GoBackAll()
    {
        if (pageStack == null)
            return;

        var target = pageStack.Pop();

        while (target.gameObject.name != "Main Menu")
            target = pageStack.Pop();

        var T = target;
        for (; !transform.IsChildOf(T); T = T.parent);

        for (int i = 0; i < T.childCount; ++i)
        {
            var current = T.GetChild(i);

            if (transform.IsChildOf(current))
            {
                current.gameObject.SetActive(false);
                break;
            }
        }

        for (; target; target = target.parent)
            target.gameObject.SetActive(true);
    }

    /// <summary>
    /// The page stack.
    /// </summary>
    static Stack<Transform> pageStack;

    /// <summary>
    /// Draws this widget.
    /// </summary>
    public virtual void Draw()
    {
        DrawBackground(drawingArea, background);
    }

    /// <summary>
    /// Called before all else.
    /// </summary>
    protected virtual void Start()
    {
        cachedArea = CalculateArea();
    }

    /// <summary>
    /// Updates this widget.
    /// </summary>
    protected virtual void Update()
    {
        var area = cachedArea = CalculateArea();
        var cursor = UIManager.fingerPosition;

        if (area.Contains(cursor))
        {
            if (Input.GetMouseButtonDown(0))
            {
                lastFingerPosition = cursor;
                inputStarted = true;
                OnFingerDown();
            }
        }

        if (inputStarted)
        {
            if (Input.GetMouseButton(0))
            {
                var delta = cursor - lastFingerPosition;

                if (delta.sqrMagnitude > .01f)
                {
                    lastFingerPosition += delta;
                    OnFingerDrag(delta);
                }
            }
            else
            {
                OnFingerUp();
                inputStarted = false;
            }
        }
    }

    /// <summary>
    /// The last cursor position.
    /// </summary>
    Vector2 lastFingerPosition;

    /// <summary>
    /// Did the input started here?
    /// </summary>
    public bool inputStarted { get; private set; }

    /// <summary>
    /// Called when the user clicks this widget.
    /// </summary>
    protected virtual void OnFingerDown()
    {
    }

    /// <summary>
    /// Called when the user releases this widget.
    /// </summary>
    protected virtual void OnFingerUp()
    {
    }

    /// <summary>
    /// Called when the user drags the cursor through this widget.
    /// </summary>
    protected virtual void OnFingerDrag(Vector2 delta)
    {
    }

    /// <summary>
    /// Is the mouse currently over?
    /// </summary>
    public bool isMouseOver { get { return drawingArea.Contains(UIManager.fingerPosition); } }

    /// <summary>
    /// The cached area.
    /// </summary>
    Rect cachedArea;

    /// <summary>
    /// Recalculate area.
    /// </summary>
    Rect CalculateArea()
    {
        Rect rect = new Rect();
        rect.position = position;
        rect.size = size;

        switch (pivot)
        {
            case TextAnchor.UpperCenter:
                rect.x -= rect.width / 2;
                break;
            case TextAnchor.UpperRight:
                rect.x -= rect.width;
                break;
            case TextAnchor.MiddleLeft:
                rect.y -= rect.height / 2;
                break;
            case TextAnchor.MiddleCenter:
                rect.x -= rect.width / 2;
                rect.y -= rect.height / 2;
                break;
            case TextAnchor.MiddleRight:
                rect.x -= rect.width;
                rect.y -= rect.height / 2;
                break;
            case TextAnchor.LowerLeft:
                rect.y -= rect.height;
                break;
            case TextAnchor.LowerCenter:
                rect.x -= rect.width / 2;
                rect.y -= rect.height;
                break;
            case TextAnchor.LowerRight:
                rect.x -= rect.width;
                rect.y -= rect.height;
                break;
        }

        return rect;
    }

    /// <summary>
    /// The area this widget should be drawn on.
    /// </summary>
    protected Rect drawingArea
    {
        get { return Application.isPlaying ? cachedArea : CalculateArea(); }
    }


    /// <summary>
    /// The screen area.
    /// </summary>
    protected Rect screenArea
    {
        get
        {
            var area = drawingArea;

            var u = 10 / Mathf.Sqrt(Screen.width * Screen.height);
            var h = new Vector2(Screen.width, Screen.height) / 2;

            area.max = area.max / u + h;
            area.min = area.min / u + h;

            return area;
        }
    }

    /// <summary>
    /// Draws a background.
    /// </summary>
    public static void DrawBackground(Rect area, Color color)
    {
        BuildMaterial();
        material.SetPass(0);

        GL.Begin(GL.TRIANGLE_STRIP);
        {
            GL.Color(color);

            GL.Vertex3(area.xMin, area.yMax, 0);
            GL.Vertex3(area.xMin, area.yMin, 0);
            GL.Vertex3(area.xMax, area.yMax, 0);
            GL.Vertex3(area.xMax, area.yMin, 0);
        }
        GL.End();
    }

    /// <summary>
    /// Checks the material.
    /// </summary>
    static void BuildMaterial()
    {
        if (material == null)
        {
            material = new Material(Shader.Find("Sprites/Default"));
            material.mainTexture = UIExtensions.whiteTexture;
        }
    }

    /// <summary>
    /// The material.
    /// </summary>
    static Material material;
}

