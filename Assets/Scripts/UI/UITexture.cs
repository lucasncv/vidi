﻿using UnityEngine;
using System.Collections;

public class UITexture : UIWidget
{
    /// <summary>
    /// The texture.
    /// </summary>
    [Header("Texture")]
    public Texture2D texture;

    /// <summary>
    /// The texture coordinates.
    /// </summary>
    public Rect textureCoordinates = new Rect(0, 0, 1, 1);

    /// <summary>
    /// Internal material.
    /// </summary>
    static Material material;

    /// <summary>
    /// Draw this widget.
    /// </summary>
    public override void Draw()
    {
        Draw(texture, drawingArea, textureCoordinates, tint);
    }

    /// <summary>
    /// Draws a texture.
    /// </summary>
    public static void Draw(Texture2D texture, Rect area, Rect sourceArea, Color tint)
    {
        BuildMaterial(texture);
        material.SetPass(0);

        GL.Begin(GL.TRIANGLE_STRIP);
        {
            GL.Color(tint);

            GL.TexCoord2(sourceArea.xMin, sourceArea.yMax);
            GL.Vertex3(area.xMin, area.yMin, 0);

            GL.TexCoord2(sourceArea.xMin, sourceArea.yMin);
            GL.Vertex3(area.xMin, area.yMax, 0);

            GL.TexCoord2(sourceArea.xMax, sourceArea.yMax);
            GL.Vertex3(area.xMax, area.yMin, 0);

            GL.TexCoord2(sourceArea.xMax, sourceArea.yMin);
            GL.Vertex3(area.xMax, area.yMax, 0);
        }
        GL.End();
    }

    /// <summary>
    /// Checks the material.
    /// </summary>
    static void BuildMaterial(Texture2D texture)
    {
        if (material == null)
            material = new Material(Shader.Find("Sprites/Default"));

        material.mainTexture = texture;
    }
}
