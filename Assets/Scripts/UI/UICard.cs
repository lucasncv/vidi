﻿using UnityEngine;
using System.Collections;

public class UICard : UIWidget
{
    /// <summary>
    /// The card database.
    /// </summary>
    [Header("Card")]
    public CardDatabase db;

    /// <summary>
    /// The card id to show.
    /// </summary>
    public int id;

    /// <summary>
    /// Draws this widget.
    /// </summary>
    public override void Draw()
    {
        if (!db) return;

        var area = drawingArea;
        var w = area.height * db.aspectRatio;

        area.xMin = area.center.x - w / 2f;
        area.width = w;

        UITexture.Draw(db[id], area, new Rect(0, 0, 1, 1), tint);
    }
}
